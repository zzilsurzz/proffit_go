
## Install

- git clone https://gitlab.com/zzilsurzz/proffit_go.git 
- cd ./proffit_go
- composer update
- npm install && npm run build
- ./vendor/bin/sail up -d
- ./vendor/bin/sail php artisan migrate
- ./vendor/bin/sail php artisan db:seed

## Swagger - http://localhost/api/documentation

## Antifriction

- Login: admin@example.com
- Password: 123
