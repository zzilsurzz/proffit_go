<?php

use App\Models\User;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::group([
    'namespace' => 'App\Http\Controllers\Admin',
    'prefix' => 'admin',
    'middleware' => ['admin']
], function () {

    Route::get('/', function() {
        return redirect()->route('admin.company.index');
    });

    Route::group(['namespace' => 'Company'], function () {
        Route::get('/company', 'IndexController')->name('admin.company.index');
        Route::get('/company/create', 'CreateController')->name('admin.company.create');
        Route::post('/company/store', 'StoreController')->name('admin.company.store');
    });

    Route::group(['namespace' => 'Car'], function () {
        Route::get('/car', 'IndexController')->name('admin.car.index');
        Route::get('/car/create', 'CreateController')->name('admin.car.create');
        Route::post('/car/store', 'StoreController')->name('admin.car.store');
        Route::get('/car/{car}/edit', 'EditController')->name('admin.car.edit');
        Route::patch('/car/{car}', 'UpdateController')->name('admin.car.update');
        Route::delete('/car/{car}', 'DestroyController')->name('admin.car.destroy');
    });

    Route::group(['namespace' => 'Property'], function() {
        Route::get('/property', 'IndexController')->name('admin.property.index');
        Route::get('/property/create', 'CreateController')->name('admin.property.create');
        Route::post('/property/store', 'StoreController')->name('admin.property.store');
    });

    Route::group(['namespace' => 'DataType'], function() {
        Route::get('/type', 'IndexController')->name('admin.data_type.index');
    });
    
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
