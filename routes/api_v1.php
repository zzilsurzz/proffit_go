<?php

use App\Http\Controllers\Api\V1\Auth\PersonalAccessTokenController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'App\Http\Controllers\Api\V1', 'middleware' => 'auth:sanctum'], function () {
    Route::group(['namespace' => 'Company'], function () {
        Route::get('/companies', 'IndexController')->name('api.company.index');
        Route::get('/companies/{company}', 'ShowController')->name('api.company.show');
        Route::post('/companies', 'StoreController')->name('api.company.store');
        Route::delete('/companies/{company}', 'DestroyController')->name('api.company.destroy');
        Route::patch('/companies/{company}', 'UpdateController')->name('api.company.update');
    });

    Route::group(['namespace' => 'Car'], function () {
        Route::get('/cars', 'IndexController')->name('api.car.index');
        Route::post('/cars', 'StoreController')->name('api.car.store');
        Route::get('/cars/{car}', 'ShowController')->name('api.car.show');
        Route::patch('/cars/{car}', 'UpdateController')->name('api.car.update');
        Route::delete('/cars/{car}', 'DestroyController')->name('api.car.destroy');
    });
    Route::group(['namespace' => 'Property'], function () {
        Route::get('/properties', 'IndexController')->name('api.property.index');
        Route::get('/properties/{property}', 'ShowController')->name('api.property.show');
        Route::post('/properties', 'StoreController')->name('api.property.store');
        Route::patch('/properties/{property}', 'UpdateController')->name('api.property.update');
        Route::delete('/properties/{property}', 'DestroyController')->name('api.property.destroy');
    });
    Route::group(['namespace' => 'CarProperty'], function () {
        Route::get('/car-properties', 'IndexController')->name('api.car_property.index');
        Route::get('/car-properties/{car_property}', 'ShowController')->name('api.car_property.show');
        Route::post('/car-properties', 'StoreController')->name('api.car_property.store');
        Route::patch('/car-properties/{car_property}', 'UpdateController')->name('api.car_property.update');
        Route::delete('/car-properties/{car_property}', 'DestroyController')->name('api.car_property.destroy');
    });

    Route::group(['namespace' => 'DataType'], function () {
        Route::get('/data-types', 'IndexController')->name('api.data_type.index');
    });
});

Route::post('/personal-access-tokens', [PersonalAccessTokenController::class, 'store']);