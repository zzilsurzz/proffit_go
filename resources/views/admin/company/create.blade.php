@extends('adminlte::page')

@section('title', 'Добавить компании')

@section('content_header')
<h1>Добавить компании</h1>
@stop

@section('content')
<form action="{{ route('admin.company.store') }}" method="POST">
    @csrf
    <div class="mb-3">
        <label for="companyName" class="form-label">Наименование компании</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" id="companyName" name="name">
        @error('name') <div id="validationCompanyNameFeedback" class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror

    </div>
    <button type="submit" class="btn btn-success">Создать</button>
    <a href="{{route('admin.company.index')}}" class="btn btn-danger">Отмена</a>
</form>
@stop