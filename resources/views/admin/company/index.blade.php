@extends('adminlte::page')

@section('title', 'Компании')

@section('content_header')
<h1>Компании</h1>
@stop

@section('content')
<div class="table-responsive">
    <table id="table1" style="width:100%" class="table table-hover table-striped">
        <thead>
            <tr>
                <th>
                    Наименование
                </th>
                <th style="width:5%" dt-no-export="">
                    <a href="{{ route('admin.company.create')}}" class="btn  btn-default text-teal mx-1 shadow d-flex nowrap align-items-center">
                        <i class="fa fa-lg fa-fw fa-plus pe-1"></i>
                        Добавить
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($companies as $company)
            <tr data-id="{{$company->id}}">
                <td>{{$company->name}}</td>
                <td>
                    <nobr>
                        <a class="btn btn-xs btn-default text-primary mx-1 shadow" title="Edit">
                            <i class="fa fa-lg fa-fw fa-pen"></i>
                        </a>
                        <button class="btn btn-xs btn-default text-danger mx-1 shadow" title="Delete">
                            <i class="fa fa-lg fa-fw fa-trash"></i>
                        </button>
                        <button class="btn btn-xs btn-default text-teal mx-1 shadow" title="Details">
                            <i class="fa fa-lg fa-fw fa-eye"></i>
                        </button>
                    </nobr>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div> {{ $companies->links() }} </div>
@stop

@section('css')
{{-- Add here extra stylesheets --}}
{{--
<link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
<script>
    console.log("Hi, I'm using the Laravel-AdminLTE package!"); 
</script>
@stop