@extends('adminlte::page')

@section('title', 'Добавить свойство')

@section('content_header')
<h1>Добавить свойство</h1>
@stop

@section('content')
<form action="{{ route('admin.property.store') }}" method="POST">
    @csrf
    <div class="mb-3">
        <label for="data_type_id" class="form-label">Тип данных</label>
        <select class="form-select form-control @error('data_type_id') is-invalid @enderror"" name="data_type_id" id="type">
            <option value="">-- Не выбрана --</option>
            @foreach ($types as $type)
            <option 
                value="{{$type->id}}"
                @selected(old('data_type_id'))>{{$type->type}}</option>
            @endforeach
        </select>
        @error('data_type_id') <div id="validationTypeFeedback" class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror

    </div>
    <div class="mb-3">
        <label for="companyName" class="form-label">Наименование</label>
        <input 
            type="text" 
            class="form-control @error('name') is-invalid @enderror" 
            id="companyName" 
            name="name"
            value="{{old('name')}}">
        @error('name') <div id="validationCompanyNameFeedback" class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror

    </div>
    <div class="mb-3">
        <div class="form-check">
            <input 
                class="form-check-input" 
                name="custom" 
                type="checkbox" 
                value="1" 
                id="custom"
                {{ old('custom') == 1 ? 'checked' : '' }}>
            <label class="form-check-label" for="custom">
                Поле с доп. логикой
            </label>
          </div>
    </div>
    <div class="mb-3">
        <div class="form-check">
            <input class="form-check-input" name="required" type="checkbox" value="1" id="required">
            <label class="form-check-label" for="required">
                Обязательно для заполнения
            </label>
          </div>
    </div>
    <button type="submit" class="btn btn-success">Создать</button>
    <a href="{{route('admin.property.index')}}" class="btn btn-danger">Отмена</a>
</form>
@stop