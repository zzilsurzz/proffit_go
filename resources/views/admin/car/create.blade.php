@extends('adminlte::page')

@section('title', 'Добавить автомобиль')

@section('content_header')
<h1>Добавить автомобиль</h1>
@stop

@section('content')
<form action="{{ route('admin.car.store') }}" method="POST">
    @csrf
    <div class="mb-3">
        <label for="companyId" class="form-label">Компании</label>
        <select class="form-select form-control @error('company_id') is-invalid @enderror"" name=" company_id"
            id="companyId">
            <option value="">-- Не выбрана --</option>
            @foreach ($companies as $company)
            <option @selected(old('company_id')) value="{{$company->id}}">{{$company->name}}</option>
            @endforeach
        </select>
        @error('company_id') <div id="validationCompanyNameFeedback" class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror

    </div>
    <div class="mb-3">
        <label for="companyName" class="form-label">Наименование авто</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" id="companyName" name="name" value="{{old('name')}}"  >
        @error('name') <div id="validationCompanyNameFeedback" class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror

    </div>
    @foreach ($properties as $property)
        @if ($property->custom)
            @continue
        @endif
        <div class="mb-3">
            @switch($property->data_type_id)
                @case(0)
                    <x-adminlte-input 
                        name="property[{{$property->id}}]" 
                        label="{{$property->name}}"
                        value="{{old('property.' . $property->id)}}"  
                        id="id-{{$property->id}}"/>
                    @break
                @case(1)
                    <x-adminlte-input
                        name="property[{{$property->id}}]" 
                        label="{{ $property->name }}"
                        value="{{old('property.' . $property->id)}}" 
                        type="number" 
                        id="id-{{$property->id}}"/>
                    @break
                @case(3)
                    <x-adminlte-textarea name="property[{{$property->id}}]" id="id-{{$property->id}}">{{old('property.' . $property->id)}}</x-adminlte-textarea>
                    @break
                @case(7)
                <div class="form-group">
                        <input type="hidden" name="property[{{$property->id}}]" value="0">
                        <label for="id-{{$property->id}}" class="mr-1">{{$property->name}}</label>
                            <input 
                                type="checkbox" 
                                id="id-{{$property->id}}" 
                                name="property[{{$property->id}}]" 
                                value="1"
                                class="@error('property.' . $property->id) is-invalid @enderror"
                                @checked(old('property.' . $property->id))>

                                @error(('property.' . $property->id)) <div id="validationCompanyNameFeedback" class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                </div>
                    @break
            @endswitch
        </div>
    @endforeach

    <button type="submit" class="btn btn-success">Создать</button>
    <a href="{{route('admin.car.index')}}" class="btn btn-danger">Отмена</a>
</form>
@stop