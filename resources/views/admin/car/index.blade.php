@extends('adminlte::page')

@section('title', 'Автомобили')

@section('content_header')
<h1>Автомобили</h1>
@stop

@section('content')
<div class="table-responsive">
    <table id="table1" style="width:100%" class="table table-hover table-striped">
        <thead>
            <tr>
                <th style="width:5%"></th>
                <th>
                    Наименование
                </th>
                <th style="width:5%" dt-no-export="">
                    <a href="{{ route('admin.car.create')}}"
                        class="btn  btn-default text-teal mx-1 shadow d-flex nowrap align-items-center">
                        <i class="fa fa-lg fa-fw fa-plus pe-1"></i>
                        Добавить
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($cars as $car)
            <tr data-id="{{$car->id}}">
                <td>
                    <button class="btn btn-dark btn-sm" type="button" data-toggle="collapse"
                    data-target="#collapse{{$car->id}}" aria-expanded="false">
                    Свойства
                </button>
                </td>
                <td>
                    <div>
                        {{$car->name}}
                    </div>
                    <div class="collapse mt-2" id="collapse{{$car->id}}">
                        <div class="card card-body">
                            <table>
                                @foreach ($car->properties as $property)
                                    <tr>
                                        <td>{{$property->name}}</td>
                                        <td>{{$property->value}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </td>
                <td style="text-align: right;">
                    <nobr>
                        <a href="{{ route('admin.car.edit', $car->id)}}" class="btn btn-xs btn-default text-primary mx-1 shadow" title="Edit">
                            <i class="fa fa-lg fa-fw fa-pen"></i>
                        </a>
                        <form action="{{ route('admin.car.destroy', $car->id)}}" method="POST" class="d-inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-xs btn-default text-danger mx-1 shadow" title="Delete" type="submit">
                                <i class="fa fa-lg fa-fw fa-trash"></i>
                            </button>
                        </form>
                    </nobr>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div> {{ $cars->links() }} </div>
@stop

@section('css')
{{-- Add here extra stylesheets --}}
{{--
<link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
<script>
    console.log("Hi, I'm using the Laravel-AdminLTE package!"); 
</script>
@stop