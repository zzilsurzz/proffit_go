@extends('adminlte::page')

@section('title', 'Типы данных')

@section('content_header')
<h1>Типы данных</h1>
@stop

@section('content')
<div class="table-responsive">
    <table id="table1" style="width:100%" class="table table-hover table-striped">
        <thead>
            <tr>
                <th>
                    ID
                </th>
                <th>
                    Тип
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($types as $type)
            <tr>
                <td>{{$type->id}}</td>
                <td>{{$type->type}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div> {{ $types->links() }} </div>
@stop

@section('css')
{{-- Add here extra stylesheets --}}
{{--
<link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
<script>
    console.log("Hi, I'm using the Laravel-AdminLTE package!"); 
</script>
@stop