<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        Event::listen(BuildingMenu::class, function (BuildingMenu $event) {
            // Add some items to the menu...
            $event->menu->add([
                'key' => 'company',
                'icon' => 'far fa-fw fa-address-card',
                'text' => 'Компании',
                'url' => route('admin.company.index'),
            ]);

            $event->menu->add([
                'key' => 'car',
                'icon' => 'fa fa-fw fa-car',
                'text' => 'Автомобили',
                'url' => route('admin.car.index'),
            ]);

            $event->menu->add([
                'key' => 'car_property',
                'icon' => 'fa fa-fw fa-bars',
                'text' => 'Свойства автомобиля',
                'url' => route('admin.property.index'),
            ]);
            $event->menu->add([
                'key' => 'data_type',
                'icon' => 'fa fa-fw fa-bars',
                'text' => 'Типы данных',
                'url' => route('admin.data_type.index'),
            ]);
        });
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
