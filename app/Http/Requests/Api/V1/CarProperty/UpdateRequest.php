<?php

namespace App\Http\Requests\Api\V1\CarProperty;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @OA\Schema(
 *  schema="CarPropertyUpdateRequest",
 *  title="Car property update request",
 *  required={"value"},
 * 
 *  @OA\Property(
 *      property="value",
 *      type="int|string|json|bool|null",
 *      example="test"
 *  ),
 * )
 */

class UpdateRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => '',
        ];
    }
}
