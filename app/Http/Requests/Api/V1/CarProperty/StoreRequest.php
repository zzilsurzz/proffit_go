<?php

namespace App\Http\Requests\Api\V1\CarProperty;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @OA\Schema(
 *  schema="CarPropertyStoreRequest",
 *  title="Car property create request",
 *  required={"name"},
 * 
 *  @OA\Property(
 *      property="car_id",
 *      type="string",
 *      example="4f5946d4-8994-43b6-a37a-f04f67326d46"
 *  ),
 *  @OA\Property(
 *      property="property_id",
 *      type="int",
 *      example="1"
 *  ),
 *  @OA\Property(
 *      property="value",
 *      type="int|string|json|bool|null",
 *      example="test"
 *  ),
 * )
 */

class StoreRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car_id' => ['required','uuid', 'exists:cars,id'],
            'property_id' => [
                'required',
                'int',
                'exists:properties,id',
                Rule::unique('car_properties', 'property_id')->where('car_id', $this->input('car_id'))
            ],
            'value' => '',
        ];
    }

    public function messages()
    {
        return [
            'property_id.unique' => 'Свойства для автомобиля должно быть уникальным',
        ];
    }

}
