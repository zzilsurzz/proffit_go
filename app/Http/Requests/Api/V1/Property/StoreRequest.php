<?php

namespace App\Http\Requests\Api\V1\Property;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *  schema="PropertyStoreRequest",
 *  title="Property create request",
 *  required={"data_type_id", "name"},
 * 
 *  @OA\Property(
 *      property="data_type_id",
 *      type="int",
 *      example="1"
 *  ),
 *  @OA\Property(
 *      property="name",
 *      type="string",
 *      example="Test property"
 *  ),
 *  @OA\Property(
 *      property="custom",
 *      type="bool",
 *      example="false"
 *  ),
 *  @OA\Property(
 *      property="required",
 *      type="bool",
 *      example="true"
 *  ),
 * )
 */
class StoreRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_type_id' => 'required|int',
            'name' => 'required|string',
            'custom' => 'nullable|bool',
            'required' => 'nullable|bool',
        ];
    }

}
