<?php

namespace App\Http\Requests\Api\V1\Car;

use App\Models\Property;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *  schema="CarStoreRequest",
 *  title="Car create request",
 *  required={"name", "company_id"},
 * 
 *  @OA\Property(
 *      property="name",
 *      type="string",
 *      example="Test car"
 *  ),
 *  @OA\Property(
 *      property="company_id",
 *      type="string",
 *      example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *  ),
 * )
 */
class StoreRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'required|string',
            'name' => 'required|string',
        ];
    }

}
