<?php

namespace App\Http\Requests\Api\V1\Car;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *  schema="CarUpdateRequest",
 *  title="Car update request",
 *  required={"name", "company_id"},
 * 
 *  @OA\Property(
 *      property="name",
 *      type="string",
 *      example="Test car 2"
 *  ),
 *  @OA\Property(
 *      property="company_id",
 *      type="string",
 *      example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *  ),
 * )
 */
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'company_id' => ['required', 'string'],
        ];
    }
}
