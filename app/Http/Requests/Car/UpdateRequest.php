<?php

namespace App\Http\Requests\Car;

use App\Models\Property;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRulesData();
    }

    /**
     * Prepare inputs for validation.
     *
     * @return void
    */    
    private function getRulesData(): array
    {
        
        $properties = Property::all();
        
        $validateData = [
            'company_id' => 'required|string',
            'name' => 'required|string',
        ];

        foreach ($properties as $property) {
            $validateData["property.{$property->id}.id"] = '';
            if ($property->custom)
                continue;

            $propertyName = "property.{$property->id}.value";
            $validateData[$propertyName] = [];

            if ($property->required) {
                $validateData[$propertyName][] = 'required';
            } else {
                $validateData[$propertyName][] = 'nullable';
            }
            $validateData[$propertyName][] = mb_strtolower($property->dataType->type);
        }

        return $validateData;
    }
}
