<?php

namespace App\Http\Resources\Api\V1\Company;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *  schema="CompanyResource",
 *  title="Company response",
 *  @OA\Property(property="data", type="array", @OA\Items(
 *      @OA\Property(
 *          property="uuid",
 *          type="string",
 *          example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="Test Company"
 *      ),
 *  )),
 * )
 */
class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'uuid' => $this->id,
            'name' => $this->name,
        ];
    }
}
