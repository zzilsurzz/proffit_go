<?php

namespace App\Http\Resources\Api\V1\DataType;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *  schema="DataTypeResource",
 *  title="Data type response",
 *  @OA\Property(property="data", type="object", 
 *      @OA\Property(
 *          property="id",
 *          type="string",
 *          example="1"
 *      ),
 *      @OA\Property(
 *          property="type",
 *          type="string",
 *          example="Int"
 *      ),
 *  ),
 * )
 */
class DataTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
        ];
    }
}
