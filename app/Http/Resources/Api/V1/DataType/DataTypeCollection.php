<?php

namespace App\Http\Resources\Api\V1\DataType;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @OA\Schema(
 *  schema="DataTypeCollection",
 *  title="Data type response collection",
 *  @OA\Property(property="data", type="array", @OA\Items(
 *      @OA\Property(
 *          property="id",
 *          type="int",
 *          example="1"
 *      ),
 *      @OA\Property(
 *          property="type",
 *          type="string",
 *          example="Int"
 *      ),
 *  )),
 * )
 */
class DataTypeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return parent::toArray($request);
    }
}
