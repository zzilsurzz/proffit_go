<?php

namespace App\Http\Resources\Api\V1\Property;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *  schema="PropertyResource",
 *  title="Property response",
 *  @OA\Property(property="data", type="object", 
 *      @OA\Property(
 *          property="id",
 *          type="int",
 *          example="1"
 *      ),
 *      @OA\Property(
 *          property="data_type_id",
 *          type="int",
 *          example="0"
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="VehicleRegNumber"
 *      ),
 *      @OA\Property(
 *          property="custom",
 *          type="bool",
 *          example="true"
 *      ),
 *      @OA\Property(
 *          property="required",
 *          type="bool",
 *          example="true"
 *      ),
 *  ),
 * )
 */
class PropertyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'data_type_id' => $this->data_type_id,
            'name' => $this->name,
            'custom' => $this->custom,
            'required' => $this->required,
        ];
    }
}
