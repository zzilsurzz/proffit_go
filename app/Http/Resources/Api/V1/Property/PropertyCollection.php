<?php

namespace App\Http\Resources\Api\V1\Property;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @OA\Schema(
 *  schema="PropertyCollection",
 *  title="Property response collection",
 *  @OA\Property(property="data", type="array", @OA\Items(
 *      @OA\Property(
 *          property="id",
 *          type="int",
 *          example="1"
 *      ),
 *      @OA\Property(
 *          property="data_type_id",
 *          type="int",
 *          example="0"
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="VehicleRegNumber"
 *      ),
 *      @OA\Property(
 *          property="custom",
 *          type="bool",
 *          example="true"
 *      ),
 *      @OA\Property(
 *          property="required",
 *          type="bool",
 *          example="true"
 *      ),
 *  )),
 * )
 */
class PropertyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return parent::toArray($request);
    }
}
