<?php

namespace App\Http\Resources\Api\V1\Car;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *  schema="CarResource",
 *  title="Car response",
 *  @OA\Property(property="data", type="object", 
 *      @OA\Property(
 *          property="uuid",
 *          type="string",
 *          example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="SITRAK"
 *      ),
 *      @OA\Property(
 *          property="company_id",
 *          type="string",
 *          example="a3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 *  ),
 * )
 */
class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'uuid' => $this->id,
            'name' => $this->name,
            'company_id' => $this->company_id,
        ];
    }
}
