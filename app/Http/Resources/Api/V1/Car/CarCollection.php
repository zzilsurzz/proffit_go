<?php

namespace App\Http\Resources\Api\V1\Car;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @OA\Schema(
 *  schema="CarCollection",
 *  title="Car response collection",
 *  @OA\Property(property="data", type="array", @OA\Items(
 *      @OA\Property(
 *          property="uuid",
 *          type="string",
 *          example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="SITRAK"
 *      ),
 *      @OA\Property(
 *          property="company_id",
 *          type="string",
 *          example="a3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 *  )),
 * )
 */
class CarCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return parent::toArray($request);
    }
}
