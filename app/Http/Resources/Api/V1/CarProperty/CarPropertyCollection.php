<?php

namespace App\Http\Resources\Api\V1\CarProperty;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @OA\Schema(
 *  schema="CarPropertyCollection",
 *  title="Car property response collection",
 *  @OA\Property(property="data", type="array", @OA\Items(
 *      @OA\Property(
 *          property="id",
 *          type="int",
 *          example="1"
 *      ),
 *      @OA\Property(
 *          property="data_type_id",
 *          type="int",
 *          example="0"
 *      ),
 *      @OA\Property(
 *          property="car_id",
 *          type="string",
 *          example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 *      @OA\Property(
 *          property="property_id",
 *          type="int",
 *          example="0"
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="VehicleRegNumber"
 *      ),
 *      @OA\Property(
 *          property="value",
 *          type="string",
 *          example="C 653 CK_58"
 *      ),
 *  )),
 * )
 */
class CarPropertyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return parent::toArray($request);
    }
}
