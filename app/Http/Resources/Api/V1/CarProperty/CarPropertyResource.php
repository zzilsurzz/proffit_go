<?php

namespace App\Http\Resources\Api\V1\CarProperty;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *  schema="CarPropertyResource",
 *  title="Car property response",
 *  @OA\Property(property="data", type="object", 
 *      @OA\Property(
 *          property="id",
 *          type="int",
 *          example="1"
 *      ),
 *      @OA\Property(
 *          property="data_type_id",
 *          type="int",
 *          example="0"
 *      ),
 *      @OA\Property(
 *          property="car_id",
 *          type="string",
 *          example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 *      @OA\Property(
 *          property="property_id",
 *          type="int",
 *          example="0"
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="VehicleRegNumber"
 *      ),
 *      @OA\Property(
 *          property="value",
 *          type="string",
 *          example="C 653 CK_58"
 *      ),
 *  ),
 * )
 */
class CarPropertyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'data_type_id' => $this->data_type_id,
            'car_id' => $this->car_id,
            'property_id' => $this->property_id,
            'name' => $this->name,
            'value' => $this->value,
        ];
    }
}
