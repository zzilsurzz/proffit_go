<?php

namespace App\Http\Controllers\Api\V1\DataType;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\DataType\DataTypeCollection;
use App\Models\DataType;

/**
 * @OA\Get(
 *      path="/api/v1/data-types",
 *      tags={"DataType"},
 *      summary="Список типов данных",
 *      security={{ "apiAuth": {} }},
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/DataTypeCollection"),
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Unauthenticated."),
 *          )
 *       ),
 * )
 */
class IndexController extends Controller
{
    public function __invoke(): DataTypeCollection
    {
        $cars = DataType::all();

        return DataTypeCollection::make($cars);
    }
}
