<?php

namespace App\Http\Controllers\Api\V1\Property;

use App\Http\Controllers\Controller;
use App\Models\Property;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Delete(
 *   path="/api/v1/properties/{property}",
 *   tags={"Property"},
 *   summary="Удалить свойство",
 *   security={{ "apiAuth": {} }},
 *       @OA\Parameter(
 *           description="property id",
 *           in="path",
 *           name="property",
 *           required=true,
 *           example="7"
 *      ),
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string",  example="done"),
 *          )
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string",  example="Unauthenticated."),
 *          )
 *       ),
 * )
 */
class DestroyController extends Controller
{
    public function __invoke(Property $property): JsonResponse
    {
        $property->delete();
        return response()->json(['message' => 'done']);
    }
}

