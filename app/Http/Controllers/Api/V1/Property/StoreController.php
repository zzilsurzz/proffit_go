<?php

namespace App\Http\Controllers\Api\V1\Property;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Property\StoreRequest;
use App\Http\Resources\Api\V1\Property\PropertyResource;
use App\Models\Property;

/**
 * @OA\Post(
 *      path="/api/v1/properties",
 *      tags={"Property"},
 *      summary="Создать свойство",
 *      security={{ "apiAuth": {} }},
 * 
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/PropertyStoreRequest")
 *      ),
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/PropertyResource")
 *       ),
 *     )
 */
class StoreController extends Controller
{
    public function __invoke(StoreRequest $request): PropertyResource
    {
        $data = $request->validated();
        $property = Property::create($data);
        return PropertyResource::make($property);
    }
}
