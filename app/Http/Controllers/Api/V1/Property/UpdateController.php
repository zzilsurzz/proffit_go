<?php

namespace App\Http\Controllers\Api\V1\Property;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Property\UpdateRequest;
use App\Http\Resources\Api\V1\Property\PropertyResource;
use App\Models\Property;

/**
 * @OA\Patch(
 *   path="/api/v1/properties/{property}",
 *   tags={"Property"},
 *   summary="Обновить данные свойства",
 *   security={{ "apiAuth": {} }},
 *       @OA\Parameter(
 *           description="property id",
 *           in="path",
 *           name="property",
 *           required=true,
 *           example="7"
 *      ),
 * 
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/PropertyUpdateRequest")
 *      ),
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/PropertyResource")
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Unauthenticated."),
 *          )
 *       ),
 * )
 */

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Property $property): PropertyResource
    {
        $data = $request->validated();
        $property->update($data);
        return PropertyResource::make($property);
    }
}
