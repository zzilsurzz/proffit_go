<?php

namespace App\Http\Controllers\Api\V1\Property;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\Property\PropertyCollection;
use App\Models\Property;

/**
 * @OA\Get(
 *      path="/api/v1/properties",
 *      tags={"Property"},
 *      summary="Список свойств",
 *      security={{ "apiAuth": {} }},
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/PropertyCollection"),
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Unauthenticated."),
 *          )
 *       ),
 * )
 */
class IndexController extends Controller
{
    public function __invoke(): PropertyCollection
    {
        $properties = Property::all();

        return PropertyCollection::make($properties);
    }
}
