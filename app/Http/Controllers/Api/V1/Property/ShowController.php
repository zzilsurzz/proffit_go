<?php

namespace App\Http\Controllers\Api\V1\Property;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\Property\PropertyResource;
use App\Models\Property;

/**
* @OA\Get(
*   path="/api/v1/properties/{property}",
*   tags={"Property"},
*   summary="Свойство",
*   security={{ "apiAuth": {} }},
*       @OA\Parameter(
*           description="ID свойства",
*           in="path",
*           name="property",
*           required=true,
*           example="1"
*      ),
* 
*      @OA\Response(
*          response=200,
*          description="OK",
*          @OA\JsonContent(ref="#/components/schemas/PropertyResource")
*       ),
* 
*       @OA\Response(
*          response=401,
*          description="FALSE",
*          @OA\JsonContent(
*              @OA\Property(property="message", type="string", example="Unauthenticated."),
*          )
*       ),
* )
*/

class ShowController extends Controller
{
    public function __invoke(Property $property): PropertyResource
    {
        return PropertyResource::make($property);
    }
}
