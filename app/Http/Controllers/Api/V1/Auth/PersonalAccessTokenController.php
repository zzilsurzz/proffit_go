<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
* @OA\SecurityScheme(
*     type="http",
*     description="Login with email and password to get the authentication token",
*     name="Token based Based",
*     in="header",
*     scheme="bearer",
*     securityScheme="apiAuth",
* ),
* 
* @OA\Post(
*      path="/api/v1/personal-access-tokens",
*      tags={"Token"},
*      summary="Получение токена",
*      @OA\RequestBody(
*           @OA\JsonContent(
*               @OA\Property(property="email", type="string", example="admin@example.com"),
*               @OA\Property(property="password", type="string", example="123"),
*          )
*      ),
*      @OA\Response(
*          response=200,
*          description="OK",
*          @OA\JsonContent(
*              @OA\Property(property="token", type="string", example="2|YaJTIVEhaVgyPPUcADQuNTvtMwOf1BcsyDUpxfq620965208awdw"),
*          )
*       ),
* )
*/
class PersonalAccessTokenController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return ['token' => $user->createToken($request->email)->plainTextToken];
    }
}
