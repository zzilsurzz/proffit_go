<?php

namespace App\Http\Controllers\Api\V1\CarProperty;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\CarProperty\CarPropertyResource;
use App\Models\CarProperty;

/**
* @OA\Get(
*   path="/api/v1/car-properties/{car_property}",
*   tags={"CarProperty"},
*   summary="Свойство автомобиля",
*   security={{ "apiAuth": {} }},
*       @OA\Parameter(
*           description="ID свойства",
*           in="path",
*           name="car_property",
*           required=true,
*           example="9a2128b4-c61f-4286-adf2-b13b4d929991"
*      ),
* 
*      @OA\Response(
*          response=200,
*          description="OK",
*          @OA\JsonContent(ref="#/components/schemas/CarPropertyResource")
*       ),
* 
*       @OA\Response(
*          response=401,
*          description="FALSE",
*          @OA\JsonContent(
*              @OA\Property(property="message", type="string", example="Unauthenticated."),
*          )
*       ),
* )
*/

class ShowController extends Controller
{
    public function __invoke(CarProperty $carProperty): CarPropertyResource
    {
        return CarPropertyResource::make($carProperty);
    }
}
