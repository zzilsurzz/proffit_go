<?php

namespace App\Http\Controllers\Api\V1\CarProperty;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\CarProperty;
use App\Models\Property;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Delete(
 *   path="/api/v1/car-properties/{car_property}",
 *   tags={"CarProperty"},
 *   summary="Удалить свойство",
 *   security={{ "apiAuth": {} }},
 *       @OA\Parameter(
 *           description="car_property uuid",
 *           in="path",
 *           name="car_property",
 *           required=true,
 *           example="0dd2ca5b-ef12-4307-b3b1-54ff90c4c320"
 *      ),
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string",  example="done"),
 *          )
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string",  example="Unauthenticated."),
 *          )
 *       ),
 * )
 */
class DestroyController extends Controller
{
    public function __invoke(CarProperty $carProperty): JsonResponse
    {
        $carProperty->delete();
        return response()->json(['message' => 'done']);
    }
}

