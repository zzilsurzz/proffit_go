<?php

namespace App\Http\Controllers\Api\V1\CarProperty;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\CarProperty\CarPropertyCollection;
use App\Models\CarProperty;

/**
 * @OA\Get(
 *      path="/api/v1/car-properties",
 *      tags={"CarProperty"},
 *      summary="Список свойств автомобиля",
 *      security={{ "apiAuth": {} }},
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/CarPropertyCollection"),
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Unauthenticated."),
 *          )
 *       ),
 * )
 */
class IndexController extends Controller
{
    public function __invoke(): CarPropertyCollection
    {
        $carProperties = CarProperty::all();

        return CarPropertyCollection::make($carProperties);
    }
}
