<?php

namespace App\Http\Controllers\Api\V1\CarProperty;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\CarProperty\UpdateRequest;
use App\Http\Resources\Api\V1\CarProperty\CarPropertyResource;
use App\Models\Car;
use App\Models\CarProperty;
use App\Models\Property;
use Illuminate\Support\Facades\Validator;

/**
 * @OA\Patch(
 *   path="/api/v1/car-properties/{car_property}",
 *   tags={"CarProperty"},
 *   summary="Обновить данные автомобиля",
 *   security={{ "apiAuth": {} }},
 *       @OA\Parameter(
 *           description="car uuid",
 *           in="path",
 *           name="car",
 *           required=true,
 *           example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 * 
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/CarPropertyUpdateRequest")
 *      ),
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/CarResource")
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Unauthenticated."),
 *          )
 *       ),
 * )
 */

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, CarProperty $carProperty): CarPropertyResource
    {
        $data = $request->validated();

        $property = $carProperty->property;

        $this->validateValueProperty($property, $data['value']);

        $carProperty->update($data);


        return CarPropertyResource::make($carProperty);
    }

    private function validateValueProperty(Property $property, $value)
    {
        $roles = [];

        if ($property->required)
            $roles[] = 'required';
        else
            $roles[] = 'nullable';

        $roles[] = mb_strtolower($property->dataType->type);

        Validator::make(
            [
                'value' => $value
            ],
            [
                'value' => $roles
            ]
        )->validate();
    }
}
