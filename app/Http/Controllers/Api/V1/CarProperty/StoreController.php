<?php

namespace App\Http\Controllers\Api\V1\CarProperty;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\CarProperty\StoreRequest;
use App\Http\Resources\Api\V1\CarProperty\CarPropertyResource;
use App\Models\Car;
use App\Models\CarProperty;
use App\Models\Property;
use Illuminate\Support\Facades\Validator;

/**
 * @OA\Post(
 *      path="/api/v1/car-properties",
 *      tags={"CarProperty"},
 *      summary="Создать свойства для автомобиль",
 *      security={{ "apiAuth": {} }},
 * 
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/CarPropertyStoreRequest")
 *      ),
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/CarPropertyResource")
 *       ),
 *     )
 */
class StoreController extends Controller
{
    public function __invoke(StoreRequest $request): CarPropertyResource
    {
        $data = $request->validated();

        $property = Property::find($data['property_id']);
        $this->validateValueProperty($property, $data['value']);

        $createData = [
            'data_type_id' => $property->data_type_id,
            'car_id' => $data['car_id'],
            'property_id' => $data['property_id'],
            'name' => $property->name,
            'value' => $data['value']
        ];

        $carProperty = CarProperty::create($createData);

        return CarPropertyResource::make($carProperty);
    }

    private function validateValueProperty(Property $property, $value)
    {
        $roles = [];

        if ($property->required)
            $roles[] = 'required';
        else
            $roles[] = 'nullable';

        $roles[] = mb_strtolower($property->dataType->type);

        Validator::make(
            [
                'value' => $value
            ],
            [
                'value' => $roles
            ]
        )->validate();
    }
}
