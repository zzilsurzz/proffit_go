<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\Car\CarCollection;
use App\Models\Car;

/**
 * @OA\Get(
 *      path="/api/v1/cars",
 *      tags={"Car"},
 *      summary="Список автомобилей",
 *      security={{ "apiAuth": {} }},
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/CarCollection"),
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Unauthenticated."),
 *          )
 *       ),
 * )
 */
class IndexController extends Controller
{
    public function __invoke(): CarCollection
    {
        $cars = Car::all();

        return CarCollection::make($cars);
    }
}
