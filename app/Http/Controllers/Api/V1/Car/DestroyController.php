<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\Controllers\Controller;
use App\Models\Car;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Delete(
 *   path="/api/v1/cars/{car}",
 *   tags={"Car"},
 *   summary="Удалить автомобиль",
 *   security={{ "apiAuth": {} }},
 *       @OA\Parameter(
 *           description="car uuid",
 *           in="path",
 *           name="car",
 *           required=true,
 *           example="4f5946d4-8994-43b6-a37a-f04f67326d46"
 *      ),
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string",  example="done"),
 *          )
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string",  example="Unauthenticated."),
 *          )
 *       ),
 * )
 */
class DestroyController extends Controller
{
    public function __invoke(Car $company): JsonResponse
    {
        $company->delete();
        return response()->json(['message' => 'done']);
    }
}

