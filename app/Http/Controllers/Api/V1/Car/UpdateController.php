<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Car\UpdateRequest;
use App\Http\Resources\Api\V1\Car\CarResource;
use App\Models\Car;

/**
 * @OA\Patch(
 *   path="/api/v1/cars/{car}",
 *   tags={"Car"},
 *   summary="Обновить данные автомобиля",
 *   security={{ "apiAuth": {} }},
 *       @OA\Parameter(
 *           description="car uuid",
 *           in="path",
 *           name="car",
 *           required=true,
 *           example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 * 
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/CarUpdateRequest")
 *      ),
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/CarResource")
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Unauthenticated."),
 *          )
 *       ),
 * )
 */

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Car $car): CarResource
    {
        $data = $request->validated();
        $car->update($data);
        return CarResource::make($car);
    }
}
