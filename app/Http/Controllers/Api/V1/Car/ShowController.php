<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\Car\CarResource;
use App\Models\Car;

/**
* @OA\Get(
*   path="/api/v1/cars/{car}",
*   tags={"Car"},
*   summary="Автомобиль",
*   security={{ "apiAuth": {} }},
*       @OA\Parameter(
*           description="ID автомобиля",
*           in="path",
*           name="car",
*           required=true,
*           example="d3ef0d28-1b82-468e-b16b-f763609b0317"
*      ),
* 
*      @OA\Response(
*          response=200,
*          description="OK",
*          @OA\JsonContent(ref="#/components/schemas/CarResource")
*       ),
* 
*       @OA\Response(
*          response=401,
*          description="FALSE",
*          @OA\JsonContent(
*              @OA\Property(property="message", type="string", example="Unauthenticated."),
*          )
*       ),
* )
*/

class ShowController extends Controller
{
    public function __invoke(Car $car): CarResource
    {
        return CarResource::make($car);
    }
}
