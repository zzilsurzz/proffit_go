<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Car\StoreRequest;
use App\Http\Resources\Api\V1\Car\CarResource;
use App\Models\Car;

/**
 * @OA\Post(
 *      path="/api/v1/cars",
 *      tags={"Car"},
 *      summary="Создать автомобиль",
 *      security={{ "apiAuth": {} }},
 * 
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/CarStoreRequest")
 *      ),
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/CarResource")
 *       ),
 *     )
 */
class StoreController extends Controller
{
    public function __invoke(StoreRequest $request): CarResource
    {
        $data = $request->validated();
        $car = Car::create($data);
        return CarResource::make($car);
    }
}
