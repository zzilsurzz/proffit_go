<?php

namespace App\Http\Controllers\Api\V1\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Company\UpdateRequest;
use App\Http\Resources\Api\V1\Company\CompanyResource;
use App\Models\Company;

/**
 * @OA\Patch(
 *   path="/api/v1/companies/{company}",
 *   tags={"Company"},
 *   summary="Обновить компанию",
 *   security={{ "apiAuth": {} }},
 *       @OA\Parameter(
 *           description="company uuid",
 *           in="path",
 *           name="company",
 *           required=true,
 *           example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 * 
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/CompanyUpdateRequest")
 *      ),
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/CompanyResource")
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Unauthenticated."),
 *          )
 *       ),
 * )
 */

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Company $company): CompanyResource
    {
        $data = $request->validated();
        $company->update($data);
        return CompanyResource::make($company);
    }
}
