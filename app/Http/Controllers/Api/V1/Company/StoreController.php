<?php

namespace App\Http\Controllers\Api\V1\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Company\StoreRequest;
use App\Http\Resources\Api\V1\Company\CompanyResource;
use App\Models\Company;

/**
 * @OA\Post(
 *      path="/api/v1/companies",
 *      tags={"Company"},
 *      summary="Создать компанию",
 *      security={{ "apiAuth": {} }},
 * 
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/CompanyStoreRequest")
 *      ),
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/CompanyResource")
 *       ),
 *     )
 */
class StoreController extends Controller
{
    public function __invoke(StoreRequest $request): CompanyResource
    {
        $data = $request->validated();
        $company = Company::create($data);
        return new CompanyResource($company);
    }
}
