<?php

namespace App\Http\Controllers\Api\V1\Company;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\Company\CompanyResource;
use App\Models\Company;

/**
* @OA\Get(
*   path="/api/v1/companies/{company}",
*   tags={"Company"},
*   summary="Компания",
*   security={{ "apiAuth": {} }},
*       @OA\Parameter(
*           description="Parameter with example",
*           in="path",
*           name="company",
*           required=true,
*           example="d3ef0d28-1b82-468e-b16b-f763609b0317"
*      ),
* 
*      @OA\Response(
*          response=200,
*          description="OK",
*          @OA\JsonContent(ref="#/components/schemas/CompanyResource")
*       ),
* 
*       @OA\Response(
*          response=401,
*          description="FALSE",
*          @OA\JsonContent(
*              @OA\Property(property="message", type="string", example="Unauthenticated."),
*          )
*       ),
* )
*/

class ShowController extends Controller
{
    public function __invoke(Company $company): CompanyResource
    {
        return new CompanyResource($company);
    }
}
