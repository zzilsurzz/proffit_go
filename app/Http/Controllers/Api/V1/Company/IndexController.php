<?php

namespace App\Http\Controllers\Api\V1\Company;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\Company\CompanyCollection;
use App\Models\Company;


 /**
 * @OA\Info(
 *     version="1.0",
 *     title="Example swagger"
 * ),
 * 
 * @OA\Get(
 *      path="/api/v1/companies",
 *      tags={"Company"},
 *      summary="Список компаний",
 *      security={{ "apiAuth": {} }},
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(
 *              @OA\Property(property="data", type="array", @OA\Items(
 *                  @OA\Property(property="id", type="string", example="d3ef0d28-1b82-468e-b16b-f763609b0317"),
 *                  @OA\Property(property="name", type="string", example="company name"),
 *              )),
 *          )
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Unauthenticated."),
 *          )
 *       ),
 * )
 */

class IndexController extends Controller
{
    public function __invoke(): CompanyCollection
    {
        return new CompanyCollection(Company::all());
    }
}
