<?php

namespace App\Http\Controllers\Api\V1\Company;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Delete(
 *   path="/api/v1/companies/{company}",
 *   tags={"Company"},
 *   summary="Удалить компанию",
 *   security={{ "apiAuth": {} }},
 *       @OA\Parameter(
 *           description="company uuid",
 *           in="path",
 *           name="company",
 *           required=true,
 *           example="d3ef0d28-1b82-468e-b16b-f763609b0317"
 *      ),
 * 
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(ref="#/components/schemas/CompanyResource")
 *       ),
 * 
 *       @OA\Response(
 *          response=401,
 *          description="FALSE",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string",  example="Unauthenticated."),
 *          )
 *       ),
 * )
 */
class DestroyController extends Controller
{
    public function __invoke(Company $company): JsonResponse
    {
        $company->delete();
        return response()->json(['message' => 'done']);
    }
}

