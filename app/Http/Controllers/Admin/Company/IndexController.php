<?php

namespace App\Http\Controllers\Admin\Company;

use App\Http\Controllers\Controller;
use App\Models\Company;

class IndexController extends Controller
{
    public function __invoke()
    {
        $companies = Company::paginate(10);

        return view('admin.company.index', compact('companies'));
    }
}
