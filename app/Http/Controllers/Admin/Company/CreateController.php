<?php

namespace App\Http\Controllers\Admin\Company;

use Illuminate\Container\Container;

class CreateController extends Container
{
    public function __invoke()
    {
        return view('admin.company.create');
    }
}
