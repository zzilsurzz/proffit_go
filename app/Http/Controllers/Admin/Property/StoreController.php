<?php

namespace App\Http\Controllers\Admin\Property;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Property;

class StoreController extends Controller
{
    public function __invoke()
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'unique:properties'],
            'data_type_id' => ['required', 'int'],
            'custom' => ['nullable', 'bool'],
            'required' => ['nullable', 'bool']
        ]);

        Property::create($data);

        return redirect()->route('admin.property.index');
    }
}
