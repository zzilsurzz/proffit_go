<?php

namespace App\Http\Controllers\Admin\Property;

use App\Http\Controllers\Controller;
use App\Models\Property;

class IndexController extends Controller
{
    public function __invoke()
    {
        $properties = Property::paginate(10);
        return view('admin.property.index', compact('properties'));
    }
}
