<?php

namespace App\Http\Controllers\Admin\Property;

use App\Http\Controllers\Controller;
use App\Models\DataType;

class CreateController extends Controller
{
    public function __invoke()
    {
        $types = DataType::all();
        return view('admin.property.create', compact('types'));
    }
}
