<?php

namespace App\Http\Controllers\Admin\DataType;

use App\Http\Controllers\Controller;
use App\Models\DataType;

class IndexController extends Controller
{
    public function __invoke()
    {
        $types = DataType::paginate(10);
        return view('admin.data_type.index', compact('types'));
    }
}
