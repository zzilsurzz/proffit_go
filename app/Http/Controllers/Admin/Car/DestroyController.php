<?php

namespace App\Http\Controllers\Admin\Car;
use App\Models\Car;

class DestroyController
{
    public function __invoke(Car $car)
    {
        $car->delete();

        return redirect()->route('admin.car.index');
    }
}
