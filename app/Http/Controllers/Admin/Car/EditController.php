<?php

namespace App\Http\Controllers\Admin\Car;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\Company;
use App\Models\Property;

class EditController extends Controller
{
    public function __invoke(Car $car)
    {
        $companies = Company::all();
        $properties = Property::all();
        $oldProperties = $this->getOldProperties($car);
        return view('admin.car.edit', compact('car', 'companies', 'properties', 'oldProperties'));
    }

    private function getOldProperties(Car $car): array
    {
        $oldProperties = [];

        foreach ($car->properties as $property) {
            $oldProperties[$property->property_id] = ['value' => $property->value, 'id' => $property->id];
        }

        return $oldProperties;
    }
}
