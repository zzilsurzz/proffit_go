<?php

namespace App\Http\Controllers\Admin\Car;

use App\Http\Controllers\Controller;
use App\Http\Requests\Car\UpdateRequest;
use App\Models\Car;
use App\Models\CarProperty;
use App\Models\Property;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Car $car)
    {
        $data = $request->validated();
        $this->update($data, $car);

        return redirect()->route('admin.car.index');
    }

    private function update(array $data, Car $car)
    {
        $propertiesData = $data['property'];
        unset($data['property']);

        $car->update($data);
        $properties = [];
        $propertiesModels = Property::all();

        foreach ($propertiesModels as $property) {
            if ($property->custom)
                continue;
            $properties[$property->id] = [
                'id' => $propertiesData[$property->id]['id'] ?? null,
                'data_type_id' => $property->data_type_id,
                'name' => $property->name,
                'value' => $propertiesData[$property->id]['value'] ?? null,
                'property_id' => $property->id,
            ]; 
        }

        $properties = collect($properties);

        $properties->each(function ($data) use ($car) {
            $car->properties()->updateOrCreate([
                'id' => $data['id'] ?? null,
            ], $data);
        });
    }
}
