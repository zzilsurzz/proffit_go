<?php

namespace App\Http\Controllers\Admin\Car;

use App\Http\Controllers\Controller;
use App\Http\Requests\Car\StoreRequest;
use App\Models\Car;
use App\Models\CarProperty;
use App\Models\Property;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $this->store($data);
        return redirect()->route('admin.car.index');
    }


    private function store(array $data)
    {
        $propertiesData = $data['property'];
        unset($data['property']);

        $car = Car::create($data);

        $properties = [];
        $propertiesModels = Property::all();

        foreach ($propertiesModels as $property) {
            
            if ($property->custom) {
                continue;
            }

            $propertyModel = new CarProperty;
            $propertyModel->data_type_id = $property->data_type_id;
            $propertyModel->name = $property->name;
            $propertyModel->value = $propertiesData[$property->id] ?? null;
            $propertyModel->property_id = $property->id;


            $properties[] = $propertyModel;
            
        }
        $car->properties()->saveMany($properties);
    }

}
