<?php

namespace App\Models\Events;

use App\Models\Car;
use App\Models\CarProperty;
use App\Models\Property;

class SetCarPropertyDisplayName
{
    public function __construct(Car $car)
    {
        if (!$displayNameProperty = Property::where('name', '=', 'DisplayName')->first())
            return;

        $vehicleRegNumber = $car->properties()->where('name', '=', 'VehicleRegNumber')->first()?->value;

        $displayName = implode(' - ', array_filter([$car->name, $vehicleRegNumber]));
        CarProperty::updateOrCreate([
            'car_id' => $car->id,
            'property_id' => $displayNameProperty->id
        ], [
            'data_type_id' => $displayNameProperty->data_type_id,
            'car_id' => $car->id,
            'property_id' => $displayNameProperty->id,
            'name' => $displayNameProperty->name,
            'value' => $displayName
        ]);
    }
}