<?php

namespace App\Models\Events;

use App\Models\Car;
use App\Models\CarProperty;
use App\Models\Property;

class CarPropertyModelDestroy
{
    public function __construct(CarProperty $carProperty)
    {
        if (!$displayNameProperty = Property::where('name', '=', 'DisplayName')->first())
            return;

        if ($carProperty->name !== 'VehicleRegNumber')
            return;

        $car = $carProperty->car;
        $displayName = $car->name;

        CarProperty::updateOrCreate([
            'car_id' => $car->id,
            'property_id' => $displayNameProperty->id
        ], [
            'data_type_id' => $displayNameProperty->data_type_id,
            'car_id' => $car->id,
            'property_id' => $displayNameProperty->id,
            'name' => $displayNameProperty->name,
            'value' => $displayName
        ]);
    }
}