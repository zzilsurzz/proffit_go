<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use App\Models\Events\CarPropertyModelDestroy;
use App\Models\Events\CarPropertyModelUpdateOrCreate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarProperty extends Model
{
    use HasFactory;
    use UsesUuid;

    protected $guarded = [];

    protected $table = 'car_properties';

    public function car() {
        return $this->belongsTo(Car::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    protected $dispatchesEvents = [
        'created' => CarPropertyModelUpdateOrCreate::class,
        'updated' => CarPropertyModelUpdateOrCreate::class,
        'deleting' => CarPropertyModelDestroy::class,
    ];
}
