<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use App\Models\Events\SetCarPropertyDisplayName;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    use UsesUuid;

    protected $guarded = [];

    public function properties() {
        return $this->hasMany(CarProperty::class);
    }
    protected $dispatchesEvents = [
        'created' => SetCarPropertyDisplayName::class,
        'updated' => SetCarPropertyDisplayName::class,
    ];
}
