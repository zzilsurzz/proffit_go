<?php

namespace Database\Seeders;

use App\Models\Property;
use Illuminate\Database\Seeder;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $types = [
            [
                'data_type_id' => 0,
                'name' => 'VehicleRegNumber',
                'custom' => false,
                'required' => true
            ],
            [
                'data_type_id' => 0,
                'name' => 'VIN',
                'custom' => false,
                'required' => false
            ],
            [
                'data_type_id' => 0,
                'name' => 'DisplayName',
                'custom' => true,
                'required' => false
            ],
            [
                'data_type_id' => 7,
                'name' => 'Fleet',
                'custom' => false,
                'required' => false
            ],
            [
                'data_type_id' => 7,
                'name' => 'LK',
                'custom' => false,
                'required' => false
            ],
        ];
        
        foreach($types as $type) {
            Property::create($type);
        }
    }
}
