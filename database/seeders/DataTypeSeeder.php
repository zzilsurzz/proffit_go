<?php

namespace Database\Seeders;

use App\Models\DataType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DataTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $types = [
            [
                'id' => 0,
                'type' => 'String'
            ],
            [
                'id' => 1,
                'type' => 'Int'
            ],
            [
                'id' => 3,
                'type' => 'Json'
            ],
            [
                'id' => 7,
                'type' => 'Bool'
            ],
        ];
        
        foreach($types as $type) {
            DataType::create($type);
        }
    }
}
