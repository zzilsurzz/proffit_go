<?php

use App\Models\Car;
use App\Models\DataType;
use App\Models\Property;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('car_properties', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->foreignIdFor(DataType::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->foreignIdFor(Car::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->foreignIdFor(Property::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->string('name');
            $table->string('value')->nullable();
            $table->timestamps();

            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('car_properties');
    }
};
